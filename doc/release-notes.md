Release Notes for Bitcoin Cash Node version 0.21.1
==================================================

Bitcoin Cash Node version 0.21.1 is now available from:

  <https://bitcoincashnode.org>
